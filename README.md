# ICTPRG434 2024 Semester 1

This is the code repository for Gordon TAFE's **ICTPRG434: Automate Processes** unit. Each week, it will be updated with resources, as well as solutions to the previous session's exercises.

For any questions, feel free to email me at <mblows@gordontafe.edu.au>.
